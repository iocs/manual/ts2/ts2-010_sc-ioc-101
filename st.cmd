#- Required modules
require ts2evm
#
#- Standard modules
require essioc
#
epicsEnvSet(PREFIX,     "TS2-010:Ctrl-EVM-101")
#
epicsEnvSet(EVM_PCI,    "09:00.0")
#
#- Load standard timing modules environment
iocshLoad "$(mrfioc2_DIR)/evm.iocsh" "P=$(PREFIX):, OBJ=EVM, PCIID=$(EVM_PCI), U=:EVRU-, D=:EVRD-"
#
#- Load TS2 EVM application
dbLoadRecords("$(ts2evm_DB)/ts2_evm_sequencer.template", "P=$(PREFIX):, EVGPREFIX=$(PREFIX):")
dbLoadRecords("$(ts2evm_DB)/ts2_evm_controls.template", "P=$(PREFIX):, EVGPREFIX=$(PREFIX):")
dbLoadRecords("$(ts2evm_DB)/ts2_nttable.template", "P=$(PREFIX):, EVGPREFIX=$(PREFIX):, R=")

#
#- Load standard ESS modules
iocshLoad("$(essioc_DIR)/common_config.iocsh")
#
#- Configure NTP provider
#- time2ntp("EVM", 2)
#
#- Creates NTTable
evmConfigNTTable $(PREFIX):
#
#- Start IOC
iocInit()
#
#- Load EVM standard after init snippet
iocshLoad("$(mrfioc2_DIR)/evg.r.iocsh","P=$(PREFIX), INTRF=, INTPPS=")
#
#- Load TS2 EVM standard init sequence 
iocshLoad("$(ts2evm_DIR)/ts2evm-init.iocsh","P=$(PREFIX):")
#
