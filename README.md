# TS2 EVM IOC

EPICS IOC for TS2 Event Master (EVM)

Based on the [e3-ts2evm](https://gitlab.esss.lu.se/rflps/e3-ts2evm.git) module: 

=======

## IOC template

This project was generated with the [E3 cookiecutter IOC template](https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-e3-ioc).
